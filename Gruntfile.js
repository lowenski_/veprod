module.exports = function(grunt){
    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },
        watch: {
            files: ['css/*.scss'],
            task: ['css']
        }
    });

    grunt.loadNpmTask('grunt-contrib-watch')
    grunt.loadNpmTask('grunt-contrib-sass');
    grunt,registerTask('css', ['sass']);
};